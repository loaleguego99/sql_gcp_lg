# sql_gcp_lg
## Descripción
El proyecto tiene como fín montar un base de datos propia de tipo MySQL 8.0 , en la nube,empleando como proveedor cloud a Google. La idea es crear una instancia empleando el producto [Cloud SQL](https://cloud.google.com/sql) de  [Google Cloud](https://cloud.google.com/). Emplearemos una cuenta gratutia de **GCP**  para uso estudiantil la cual nos regala **300 dolares** para probar sus productos en un lapso de 3 meses.

Para realizar la conexión a nuestra instancia en la nube desde nuestra maquina local emplearemos la herramienta open-source [MySQL Workbench](https://www.mysql.com/products/workbench/)

**Se mostrara la siguiente información**
- Paso a paso para crear la instancia con sus respectivas configuraciones que permiten optimizar los recursos (Costo x día **$3.5 dolares**)
- Script de MySQL para crear una base de datos ficticia de una empresa dedicada a la venta de modelos a escala de medios de transporte
- Consultas SQL que permiten sacar insights y responder a KPIs de la empresa


## Requerimientos previos
- Cuenta GCP con mínimo 5 dolares de saldo
![gcp](/uploads/811293dc17b6bdd2b96abccf9834f265/gcp.jpg)

- Instalar [MySQL Workbench](https://dev.mysql.com/downloads/workbench/) de manera local.
![mysql](/uploads/8d01beaae7b3ebb0c5ded856ff28b05e/mysql.png)

## Getting started
## Paso 1 Crear instancia  y respectivas configuraciones
- Vamos al aparto de SQL el cual lo podemos ubicar en el menu desplegable de la parte izquierda de la pantalla.
![sql](/uploads/f80845b80c2185dabedd27c335975342/sql.PNG)
- Si es la primera vez nos aparece habilitar API, debemos clickear en este icono y esperar 5 minutos para luego proceder a seguir con nuestro siguiente paso.
- Luego procedemos a darle click y nos aparece el menu principal donde salen las instancias que hemos creado hasta el momento , en la parte superior le buscamos **+ CREAR INSTANCIA**
![lolito](/uploads/6de2797ada58604d508b023db01cd7f8/lolito.PNG)
-Nos muestran las tipo de bases de datos disponibles que podemos crear ya sea  **MySQL**,PostgreSQL o SQLServer. Para nuestro ejemplo utilizaremos MySQL y la versión a elegir sera **8.0**
- Ya entramos al menu de creación de la instancia, donde debemos rellenar los siguientes datos:
	- ID de instancia : id unico el cual debe contener solo letras minusculas,números y guiones, a su vez debe empezar siempre por una letra
	- Contraseña
	- Versión de la base de datos : MySQL 8.0
	- Configuración : Debe ser de tipo **Desarrollo** para que los gastos sean mas bajos.
	- Región : us-west4(Las vegas)
	- Disponibilidad zonal : Zona única
- Luego en el apartado de **Personaliza tu instancia** damos click y se nos abre un sub-menu el cual clickleamos en **Tipo de máquina** donde debemos personalizar la siguiente información:
![memoria_conf](/uploads/d8a49ae938c832af8e1fffa1dbb3f3f6/memoria_conf.PNG)
- Finalmente le damos en crear. La creación dura aproximadamente 5 minutos por ende debemos ser pacientes y no interrumpir su creación.
- Procedemos a clickear en el icono de SQL para ver las instancias que hemos creado , les debe aparecer su instancia creada , donde sale el respectivo nombre, el tipo, la dirección pública y entre otra información relevante.
![instancia](/uploads/06681fc154239d6f3c139c574c4446e6/instancia.PNG)
## Paso 2 Configuración para accederla remotamente
Vamos a realizar una configuración el cual permite que cualquier cliente IPv4 cruce el firewall de la red y pueda acceder a la instancia, tener en cuenta que esto no es una buena **práctica** , en un ambiente de producción se debe limitar las IP's que puedan acceder, pero para fines prácticos de este proyecto no crearemos ninguna regla o lista de acceso para limitar el trafico hacia esta instancia
- Debemos clickear la instancia que creamos y luego ir al apartado de conexiones el cual esta ubicado a la mano izquierda del menu principal
![conexion](/uploads/0db0e1d9968ae1742ad8078485ccf40f/conexion.PNG)
Clickeamos sobre el apartado de **Herramientas de red**
![red](/uploads/b13eb6644fdcab904a3798bb01387b63/red.PNG)
Luego en la sección de **Redes autorizadas** procedemos agregar una red con las siguientes caracteristicas:
	-Nombre:internet (puede ser el que ustedes deseen)
	Dirección: 0.0.0.0/0
Finalmente pickeamos en guardar.
## Paso 3 Creación de cuenta de usuario
Es posible conectarnos a nuestra instancia previamente creada utilizando el usuario root que viene por defecto usando la contraseña que creamos en el **Paso 1** de este tutorial.
Siempre se deben crear perfiles de usuarios, cada uno con permisos distintos. Según sea su proposito(consultar,escribir, eliminar o actualizar) esto con el fin de preservar la seguridad de la base de datos.
- Para crear un usuario nuevo debemos ir al apartado de usuarios ubicado a mano izquierda de la pantalla.
![user](/uploads/c7f2030148bfc26c120a42fd9ccbcb92/user.PNG)
- Luego damos en **AGREGAR CUENTA DE USUARIO**
![cuenta_user](/uploads/ac13a4631fa2d4847e5a5e0c4e2a5b54/cuenta_user.PNG)
- Procedemos a escribir un nombre de usuario con su respectiva contraseña, ademas habilitamos la opción de permitir cualquier host (%)
![user_lg](/uploads/6d079d748d16e6f83458476f28600e04/user_lg.PNG)
Los usuarios creados usando CLOUD SQL tienen los mismos privilegios que el **cloudsqlsuperuse** , los privilegios pueden ser cambiados usando los comandos **GRANT** o **REVOKE**. Este "role" adquirido automaticamente nos da todos los privilegios estaticos exceptos **SUPER** y **FILE**.

Por ende es posible realizar las siguientes acciones concretas para nuestro proyecto sin ningún problema:
- DDL (Crear,eliminar y alterar tablas)
- DML (Insertar,actualizar y eliminar registros)
##  Paso 4 Conexión a nuestra instancia  usando MySQL  Workbrench
- Para realizar este paso, previamente debemos saber la dirección IP publica de nuestra instancia, la cual la podemos saber dando click sobre esta misma. Debemos buscar el subtitulo que dice **Conectarse a esta instancia**, donde encontraremos la dirección que estamos necesitando.
![ip](/uploads/13a3b1bb48d18f823317ffb3632ae298/ip.PNG)

- Estando en el home de la aplicación Workbrench, procedemos a clickear en el siguiente icono.
![test_conection](/uploads/60f5a9f4b29f5abd0febb7f6194f08d4/test_conection.PNG)
- Nos sale una ventana donde crearemos nuestra conexión:
	- Connection Name : SQL_GCP20user(puede ser el que uno quiera)
	- Connection Method : Standard (TCP/IP)
	- hostname: Dirección IP publica de nuestra instancia en GCP
	- username: usuarios_base_datos_lg(usuario creado en el paso anterior)
Finalmente le damos a Test Connectión donde nos solicitan el password del usuario previamente creado.
Encontramos dentro de mis conexiones , la conexión creada hace un momento. Procedemos a darle doble click y ya finalmente estaremos conectados a nuestra instancia SQL alojada en la nube.
![conectarsePNG](/uploads/1a46f291a345fea0c41074d3954358ce/conectarsePNG.PNG)
## Paso 5 creación de base de datos e inserción de registros
En el repositorio existe un archivo llamado **mysqlsampledatabase.sql**, se debe proceder a descargarlo y luego abrirlo en nuestra conexión establecida.
Le damos en File y luego en Open SQL Script y seleccionaremos la ruta del archivo donde fue descargado.
![execute_sql](/uploads/7500a6839e83d9c0a6e4aeeb60ffb770/execute_sql.PNG)
El nombre de la base de datos viene por defecto llamado "l_guerrero" pero cada quien es libre de renombrarla como desee.
Se deben modificar las palabras resaltadas en rojo de la imagen
![modifi_sql](/uploads/716ccf98770dfd8c9e81037a82ce2fda/modifi_sql.PNG)
Finalmente procedemos a ejecutar la query,empleando el primer icono de color amarillo que simula un rayo.
![outputxd](/uploads/35517c324471adc14d881e6fe4c2c977/outputxd.PNG)
Ahora al actualizar nuestros esquemas podremos encontrar la base de datos llamada **l_guerrero**.
![base_datos](/uploads/a9ef72dc4d0a5392f9cb0c3d1f9fb213/base_datos.PNG)

## Paso 6 Consultas SQL 
El esquema de nuestra base de datos es el siguiente:
![esquema](/uploads/0f1365c52cf32ccd0dc64eec88fd54b5/esquema.PNG)
Se adjunta archivo llamado consultas_lg.sql de consultas SQL respecto al modelo y una explicación de su accionar.
Lo que se busca en las Consultas SQL es extraer insight(valor de la información) para asi poder evaluar un KPI(indicador) del comportamiento de alguna area de negocio de una compañia.

El KPI que vamos a evaluar son las ventas totales realizadas por los representantes de ventas en un tiempo en especifico.

## Explicación de la consulta
Un ejemplo de consulta nos permite obtener el total de ventas generadas por cada representante de ventas. Esto nos ayuda a realizar un seguimiento individual del desempeño y evaluar su contribución en las ventas totales de la compañía. El análisis de cada rendimiento nos permite sacar conclusiones y, si es necesario, brindar el apoyo o entrenamiento adecuado para mejorar. El monitoreo de las ventas y el cumplimiento de los objetivos es fundamental para tomar decisiones oportunas.
```
SELECT e.employeeNumber,CONCAT(e.firstName,' ',e.lastName) AS full_name,SUM(p.amount) AS total_ventas,COUNT(1) AS cantidad_payments FROM classicmodels2.employees AS e
INNER JOIN classicmodels2.customers AS c ON e.employeeNumber = c.salesRepEmployeeNumber
INNER JOIN classicmodels2.payments AS p ON c.customerNumber = p.customerNumber
GROUP BY full_name,e.employeeNumber
ORDER BY total_ventas DESC
```
![consulta_sql](/uploads/4e031cbb63b57b5dfdcf7aa53cfd7fb1/consulta_sql.PNG)

## Support
Para alguna duda o inquietud contactarse al siguiente correo electronico
loaleguego99@gmail.com
## Autores
Proyecto realizado por Lorenzo Guerrero
[![Linkedin](https://i.stack.imgur.com/gVE0j.png) LinkedIn](https://www.linkedin.com/lorenzoguerrero17)
&nbsp;



