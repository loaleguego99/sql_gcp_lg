#Numero de empleados x  ciudad
SELECT o.officeCode,city,COUNT(1) AS cantidad FROM l_guerrero.offices AS o
INNER JOIN employees AS e ON o.officeCode = e.officeCode
GROUP BY o.officeCode
ORDER BY cantidad DESC
#Número de empleados x pais
SELECT o.country,COUNT(1) AS cantidad FROM l_guerrero.offices AS o
INNER JOIN employees AS e ON o.officeCode = e.officeCode
GROUP BY o.country
ORDER BY cantidad DESC
#Número de empleados x cargo ordenados de forma descendente segun la cantidad
SELECT DISTINCT(jobTitle),COUNT(1) AS cantidad FROM l_guerrero.employees
GROUP BY jobTitle
ORDER BY cantidad DESC
#Número de empleados a cargo
SELECT CONCAT(e.firstName,' ',e.lastName) AS full_name,e.jobTitle,COUNT(1) AS cantidad_subordinados FROM l_guerrero.employees AS e
INNER JOIN l_guerrero.employees AS rp ON e.employeeNumber = rp.reportsTo
GROUP BY full_name,e.jobTitle,e.firstName
ORDER BY cantidad_subordinados DESC
#Clientes sin credito
SELECT * FROM l_guerrero.customers 
WHERE creditLimit = 0
#Numero de clientes x representante de ventas
SELECT COALESCE(CONCAT(e.firstName,' ',e.lastName),'Sin representante') AS full_name,COUNT(1) AS cantidad_clientes FROM l_guerrero.employees AS e
RIGHT JOIN l_guerrero.customers AS c ON e.employeeNumber = c.salesRepEmployeeNumber
GROUP BY full_name
ORDER BY cantidad_clientes DESC
#Número de clientes x país
SELECT country,COUNT(1) AS cantidad FROM l_guerrero.customers
GROUP BY country
ORDER BY cantidad DESC
#Número de clientes x ciudad
SELECT city,COUNT(1) AS cantidad FROM l_guerrero.customers
GROUP BY city
ORDER BY cantidad DESC
#Top 10 clientes con mas ordenes que fueron enviadas sin ningun problema
SELECT c.customerName, COUNT(1) AS cantidad_ordenes FROM l_guerrero.customers AS c
INNER JOIN l_guerrero.orders AS o ON c.customerNumber = o.customerNumber
WHERE status = 'Shipped'
GROUP BY c.customerName
ORDER BY cantidad_ordenes DESC
LIMIT 10
#Pagos totales realizados por clientes
SELECT c.customerName, SUM(amount) AS total_pagos FROM l_guerrero.customers AS c
INNER JOIN l_guerrero.payments AS p ON c.customerNumber = p.customerNumber
GROUP BY c.customerName
ORDER BY total_pagos DESC
#Top 10 Paises que mas dínero generan
SELECT c.country, SUM(amount) AS total_pagos FROM l_guerrero.customers AS c
INNER JOIN l_guerrero.payments AS p ON c.customerNumber = p.customerNumber
GROUP BY c.country
ORDER BY total_pagos DESC
LIMIT 10
#Pagos agrupados x año ordenados de mayor a menor
SELECT YEAR(paymentDate) AS año , SUM(amount) AS total_pagos FROM l_guerrero.payments
GROUP BY año
ORDER BY total_pagos DESC
#Pagos agrupados x mes ordenados de mayor a menor
SELECT MONTHNAME(paymentDate) AS mes , SUM(amount) AS total_pagos FROM l_guerrero.payments
GROUP BY mes
ORDER BY total_pagos DESC
#Cantidad de productos_codigo x orden
SELECT orderNumber,MAX(orderLineNumber) AS cantidad FROM orderdetails 
GROUP BY orderNumber
#Cantidad de productos totales x orden
SELECT orderNumber, SUM(quantityOrdered) AS cantidad_productos FROM orderdetails 
GROUP BY orderNumber
#Orden y su respectivo valor en dolares
SELECT tabla.orderNumber,SUM(tabla.total) AS total_orden FROM (SELECT orderNumber,quantityOrdered*priceEach AS total  FROM orderdetails) AS tabla
GROUP BY tabla.orderNumber
#Productos mas vendidos
SELECT productName,p.productCode,COUNT(1) AS cantidad FROM orderdetails AS od
INNER JOIN products AS p ON od.productCode = p.productCode
GROUP BY p.productName,p.productCode
ORDER BY cantidad DESC
#Vendedor con mas productos
SELECT productVendor,COUNT(1) AS cantidad FROM l_guerrero.products
GROUP BY productVendor
#Pagos totales agrupados x clientes
SELECT c.customerNumber,c.customerName,c.city,SUM(p.amount) AS pago_total FROM l_guerrero.customers AS c
INNER JOIN l_guerrero.payments AS p ON c.customerNumber = p.customerNumber
GROUP BY c.customerName,c.customerNumber,c.city
#Pago total agrupado x ciudad
SELECT c.city,SUM(p.amount) AS total_venta FROM l_guerrero.customers AS c
INNER JOIN l_guerrero.payments AS p ON c.customerNumber = p.customerNumber
GROUP BY c.city
ORDER BY c.city ASC
#Total de ventas x empleados ordenados de mayor a menor
SELECT e.employeeNumber,CONCAT(e.firstName,' ',e.lastName) AS full_name,SUM(p.amount) AS total_ventas,COUNT(1) AS cantidadPagosAsociados FROM l_guerrero.employees AS e
INNER JOIN l_guerrero.customers AS c ON e.employeeNumber = c.salesRepEmployeeNumber
INNER JOIN l_guerrero.payments AS p ON c.customerNumber = p.customerNumber
GROUP BY full_name,e.employeeNumber
ORDER BY total_ventas DESC
#Total de ventas x ciudad ordenados de mayor a menor
SELECT o.officeCode,o.city,SUM(p.amount) AS total_ventas,COUNT(1) AS cantidadPagosAsociados FROM l_guerrero.employees AS e
INNER JOIN l_guerrero.offices AS o ON e.officeCode = o.officeCode
INNER JOIN l_guerrero.customers AS c ON e.employeeNumber = c.salesRepEmployeeNumber
INNER JOIN l_guerrero.payments AS p ON c.customerNumber = p.customerNumber
GROUP BY o.officeCode,o.city
ORDER BY total_ventas DESC
#Cantidad de productos x linea de productos
SELECT pl.productLine,COUNT(1) AS cantidad FROM l_guerrero.products AS p
INNER JOIN l_guerrero.productlines AS pl ON p.productLine = pl.productLine
GROUP BY pl.productLine
#Cantidad de productos x linea_De_productos en Ordenes
SELECT pl.productLine,COUNT(1) AS cantidad FROM l_guerrero.products AS p
INNER JOIN l_guerrero.orderdetails AS o ON p.productCode = o.productCode
INNER JOIN l_guerrero.productlines AS pl ON p.productLine = pl.productLine
GROUP BY pl.productLine

#Cantidad de Dínero generado x producto
SELECT p.productName,tabla_producto_order.productCode,SUM(tabla_producto_order.total) AS total_venta  FROM (SELECT productCode,quantityOrdered*priceEach AS total  FROM orderdetails) AS tabla_producto_order
INNER JOIN products AS p ON tabla_producto_order.productCode = p.productCode
GROUP BY p.productName,tabla_producto_order.productCode

#Cantida de dínero generado x linea de _productos
SELECT pl.productLine,SUM(tabla_producto_order.total) AS dinero_generado  FROM (SELECT productCode,quantityOrdered*priceEach AS total  FROM orderdetails) AS tabla_producto_order
INNER JOIN products AS p ON tabla_producto_order.productCode = p.productCode
INNER JOIN productlines AS pl ON p.productLine = pl.productLine
GROUP BY pl.productLine
ORDER BY dinero_generado DESC
